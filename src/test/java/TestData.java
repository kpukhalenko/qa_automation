import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestData {
//    static Helper h;

    /* basic settings */
    static String host = "http://jira.hillel.it:8080/";

    /* current date and time */
    static String getDateAndTime() {
        return new SimpleDateFormat("dd_MM_yyyy-HH_mm_ss").format(new Date());
    }

    /* issue section */
    static String issueID;
    static String summary = "Autotest-KP-" + getDateAndTime();

    /* login section */
    static String user_1_Username = "autorob";
    static String user_1_Password = "forautotests";

    /* Admin create user */
    static String createUserEmail = "KP-" + getDateAndTime() + "@gmail.com";;
    static String createUserFullName = "KP-" + getDateAndTime() ;
    static String createUserUserName = "KP-" + getDateAndTime() ;

    /* file attachment */
    static String filePath;
    static String fileName;
    static String downloadPath;

    static void setEnvironment(String environment) {
        switch(environment) {
            case "PC_Work":
                filePath = "c:\\Users\\kostiantyn.pu\\Documents\\KoSS_files\\Work\\Learning\\qa_automation\\src\\main\\resources\\TestData\\";
                fileName = "Screenshot-000019.jpg";
                downloadPath = "c:\\Users\\kostiantyn.pu\\Downloads\\";
                break;
            case "notebook":
                filePath="c:\\KoSS\\Work\\learning\\QA_Automation\\IdeaProjects\\QA_Automation\\src\\main\\resources\\TestData\\";
                fileName = "1540238129030.png";
                downloadPath = "C:\\Загрузки\\";
                break;
        }
    }

}
