import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.Test;

import java.io.File;
import java.time.Duration;

public class JiraActions {

    private WebDriver browser;
    private Helper h;

    public JiraActions(WebDriver browser) {
        this.browser = browser;
        this.h = new Helper(browser);
    }

    @FindBy (css = "input[id='login-form-username']") WebElement fieldLogin;
    @FindBy (css = "input[id='login-form-password']") WebElement fieldPassword;
    @FindBy (css = "input[id='login']") WebElement buttonLogin;
    @FindBy (css = "a[id='create_link']") WebElement buttonCreateIssue;
    @FindBy (css = "input[id='project-field']") WebElement fieldProject;
    @FindBy (css = "input[id='issuetype-field']") WebElement fieldIssueType;
    @FindBy (css = "input[id='reporter-field']") WebElement fieldReporter;
    @FindBy (css = "a[class='issue-created-key issue-link']") WebElement selectorCreatedIssue;
    @FindBy (css = "input[id='summary']") WebElement fieldSummary;
    @FindBy (css = "input[class='issue-drop-zone__file ignore-inline-attach']") WebElement fieldUploadAttachment;
    @FindBy (css = "a[id='cp-control-panel-download']") WebElement buttonDownloadAttachment;
    @FindBy (css = "#admin_menu") WebElement buttonAdministration;
    @FindBy (css = "#admin_users_menu") WebElement buttonUserManagement;
    @FindBy (css = "#login-form-authenticatePassword") WebElement inputPasswordAuthentication;
    @FindBy (css = "a[id='create_user']") static WebElement buttonCreateUser;

    @FindBy (css = "input[id='user-create-email']") static WebElement inputCreateUserEmail;
    @FindBy (css = "input[id='user-create-fullname']") static WebElement inputCreateUserFullName;
    @FindBy (css = "input[id='user-create-username']") static WebElement inputCreateUserUserName;
//    @FindBy (css = "id='password'") static WebElement inputCreateUserPassword; //not required field



    static String selectorIssueKey = "data-issue-key";

    void login(String username, String password) {
        browser.get(TestData.host);
        h.clearAndFillByFind(fieldLogin, username);
        h.clearAndFillByFind(fieldPassword, password);
        buttonLogin.click();
    }

    void createIssue() {
        buttonCreateIssue.click();

        h.clearAndFillByFind(fieldProject, "General QA Robert (GQR)\n");
        new FluentWait<>(browser)
                .withTimeout(Duration.ofSeconds(5))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(InvalidElementStateException.class)
                .until(
                        browser -> h.clearAndFillByFind(fieldIssueType, "Test\n")
                );
        new FluentWait<>(browser)
                .withTimeout(Duration.ofSeconds(5))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(InvalidElementStateException.class)
                .until(
                        browser -> h.clearAndFillByFind(fieldReporter, "Automation Robert\n")
                );
        new FluentWait<>(browser)
                .withTimeout(Duration.ofSeconds(5))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(InvalidElementStateException.class)
                .until(
                        browser -> h.clearAndFillByFind(fieldSummary, TestData.summary + "\n")
                );

        TestData.issueID = selectorCreatedIssue.getAttribute(selectorIssueKey);
    }

    void uploadAttachment(String filePath, String fileName) {
        fieldUploadAttachment.sendKeys(filePath + fileName);
    }

    void downloadAttachment() throws Exception {
        browser.findElement(By.linkText(TestData.fileName)).click();
        Thread.sleep(5000);
        buttonDownloadAttachment.click();
    }

    boolean isFileDownloaded(String downloadPath, String fileName) {
        boolean flag = false;
        File dir = new File(downloadPath);
        File[] dir_contents = dir.listFiles();
        for (int i = 0; i < dir_contents.length; i++) {
            if (dir_contents[i].getName().equals(fileName))
                return flag=true;
        }
        return flag;
    }

    void openUserManagement() {
        buttonAdministration.click();
        buttonUserManagement.click();
        if (inputPasswordAuthentication.isDisplayed()) {
            h.clearAndFillByFind(inputPasswordAuthentication, TestData.user_1_Password + "\n");
        }
    }

    void createUser() {
        browser.get(TestData.host + "/secure/admin/user/UserBrowser.jspa");
        buttonCreateUser.click();
        h.clearAndFillByFind(inputCreateUserEmail, TestData.createUserEmail);
        h.clearAndFillByFind(inputCreateUserFullName, TestData.createUserFullName);
        h.clearAndFillByFind(inputCreateUserUserName, TestData.createUserUserName + "\n");
    }

}
