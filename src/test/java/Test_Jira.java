import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;

public class Test_Jira extends TestBase {

    static JiraActions steps;

    @BeforeClass
    private static void init() {
        steps = PageFactory.initElements(browser, JiraActions.class);
        PageFactory.initElements(browser,Test_Jira.class);
    }

    @FindBy (css = "a[id='create_link']") static WebElement createIssueButton;
    @FindBy (css = "div#usernameerror") static WebElement loginErrorMessage;
    @FindBy (css = "a[class='issue-created-key issue-link']") static WebElement messageIssueCreated;
    @FindBy (css = "h1#summary-val") static WebElement fieldSummary;
    @FindBy (css = "div[class='aui-message aui-message-success success closeable shadowed aui-will-close']") static WebElement messageAttachmentUploaded;
    @FindBy (css = "a[class='attachment-title']") static WebElement fieldAttachment;
    @FindBy (css = "div[class='generalAdminHeading aui-page-header']") static WebElement adminHeader;
    @FindBy (css = "a[href='/secure/admin/user/UserBrowser.jspa']") static WebElement adminUserManagementHeader;
    @FindBy (css = "a[id='create_user']") static WebElement buttonCreateUser;
    @FindBy (css = "span[class='user-created-flag-single']") static WebElement messageUserCreated;


    @Test (priority = -1, description = "User should NOT login")
    static void loginFail() throws Exception {
        steps.login("failed_username", "failed_password");
        Assert.assertTrue(loginErrorMessage.isDisplayed());
    }

    @Test (description = "User should successfully login")
    static void loginSuccess() throws Exception {
        steps.login(TestData.user_1_Username, TestData.user_1_Password);
        Assert.assertTrue(createIssueButton.isDisplayed());
    }

    @Test (dependsOnMethods = "loginSuccess", description = "Creating an issue from pop-up")
    static void createIssue() {
        steps.createIssue();
        Assert.assertTrue(messageIssueCreated.isDisplayed());
    }

    @Test (dependsOnMethods = "createIssue", description = "opens recently created issue and verifies its summary")
    static void openIssue() throws Exception{
        browser.get(TestData.host + "browse/" + TestData.issueID);
        Assert.assertEquals(TestData.summary, fieldSummary.getText());
    }

    @Test (dependsOnMethods = "openIssue")
    static void uploadAttachment() {
        steps.uploadAttachment(TestData.filePath, TestData.fileName);
        Assert.assertTrue(messageAttachmentUploaded.isDisplayed());
        Assert.assertTrue(messageAttachmentUploaded.getText().contains(TestData.fileName + " was attached successfully to "));
        Assert.assertEquals(fieldAttachment.getAttribute("file-preview-title"), TestData.fileName);
    }

    @Test (dependsOnMethods = "uploadAttachment")
    public void verifyDownloadWithFileName() throws Exception {
        steps.downloadAttachment();
        Assert.assertTrue(steps.isFileDownloaded(TestData.downloadPath, TestData.fileName), "Failed to download Expected document");
    }

    @Test (dependsOnMethods = "loginSuccess")
    void accessAdminArea() {
        steps.openUserManagement();
        Assert.assertTrue(adminHeader.isDisplayed());
        Assert.assertTrue(buttonCreateUser.isDisplayed());
    }

    @Test (dependsOnMethods = "accessAdminArea")
    void createUserSuccess() {
        steps.createUser();
        Assert.assertTrue(messageUserCreated.isDisplayed());
    }
}
