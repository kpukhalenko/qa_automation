import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver browser;
    public static Helper h;

    @BeforeTest
    public static void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe"); //должно быть объявлено перед запуском chromedriver
        browser = new ChromeDriver();

        browser.manage().window().maximize();
        browser.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);

        h = new Helper(browser);
        TestData.setEnvironment(/*"notebook"*/"PC_Work");
    }

//    @AfterTest
    public static void closeBrowser() { browser.quit(); }

}
