import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Helper {

    private WebDriver browser;

    Helper(WebDriver currentBrowser) {
        browser = currentBrowser;
    }

    WebElement clearAndFillBySelector(By selector, String data) {
        WebElement element = browser.findElement(selector);
        element.clear();
        element.sendKeys(data);
        return element;
    }

    WebElement clearAndFillByFind(WebElement element, String data) {
        element.clear();
        element.sendKeys(data);
        return element;
    }

}
